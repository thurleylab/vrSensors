import usb
import ctypes


class Mouse(object):
    """
    Represents an optical mouse connected via USB.
    Provides methods to stream mouse position coordinates (x, y)
    to the shared memory location (Point).
    """
    report_timeout = 100  # ms

    class _MouseFilter(object):

        def __call__(self, device):
            for cfg in device:
                itf = usb.util.find_descriptor(cfg, bInterfaceClass=3, bInterfaceProtocol=2)
                if itf is not None:
                    return True

            return False

    def __init__(self, usb_device):
        self.usb_device = usb_device

        cfg = self.usb_device[0]
        itf = usb.util.find_descriptor(cfg, bInterfaceClass=3, bInterfaceProtocol=2)
        self._endpoint_in = itf.endpoints()[0]

    def __str__(self):
        return str(self.usb_device)

    def __repr__(self):
        return repr(self.usb_device)

    def _read_report(self, size=None, timeout=None):
        size_f = size or self._endpoint_in.wMaxPacketSize
        timeout_f = timeout or self.report_timeout
        try:
            return self._endpoint_in.read(size_f, timeout_f)

        except usb.USBError:
            return None

    def detach(self):
        try:
            if self.usb_device.is_kernel_driver_active(0):
                self.usb_device.detach_kernel_driver(0)
        except NotImplementedError:  # not implemented on Win
            pass

    def get_position_change(self):
        report = self._read_report()
        if report:
            x = float(ctypes.c_int8(report[1]).value)
            y = float(ctypes.c_int8(report[2]).value)
            return x, y
        return 0., 0.

    @staticmethod
    def list_connected(**kwargs):
        all_mice = list(usb.core.find(find_all=True, custom_match=Mouse._MouseFilter()))
        sensors = [Mouse(x) for x in all_mice]

        for key, value in kwargs.items():
            if value is not None:
                sensors = list(filter(lambda x: getattr(x.usb_device, key, None) == value, sensors))

        return sensors
