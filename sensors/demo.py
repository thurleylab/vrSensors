import ctypes
import usb
import Tkinter
import time
import sys
import json

from multiprocessing import Process
from multiprocessing.sharedctypes import Value
from sensors.mouse import Mouse


class DemoApp:

    def __init__(self, font_size=40):
        self.master = Tkinter.Tk()

        self.x = Tkinter.StringVar()
        self.x.set("0.0")
        self.y = Tkinter.StringVar()
        self.y.set("0.0")

        self.dx = Tkinter.StringVar()
        self.dx.set("0.0")

        self.dy = Tkinter.StringVar()
        self.dy.set("0.0")

        Tkinter.Label(self.master, textvariable=self.x, font=("Helvetica", font_size)).pack()
        Tkinter.Label(self.master, textvariable=self.y, font=("Helvetica", font_size)).pack()
        Tkinter.Label(self.master, textvariable=self.dx, font=("Helvetica", font_size)).pack()
        Tkinter.Label(self.master, textvariable=self.dy, font=("Helvetica", font_size)).pack()


def sensor_handler(dev_kwargs, frequency, x, y, dx, dy, stop_trigger):
    """
    Updates x, y (memory-shared) coordinates with actual mouse
    position with a given frequency.

    :param dev_kwargs:      query dict to get the device, like {'bus': 0, 'address': 3, ...}
    :param frequency:       update frequency in Hz
    :param x:               var to update x (multiprocessing.sharedctypes.Value)
    :param y:               var to update y (multiprocessing.sharedctypes.Value)
    :param stop_trigger:    shared Value of ctypes.c_bool (multiprocessing.sharedctypes.Value)
    """
    sensor = Mouse.list_connected(**dev_kwargs)[0]

    try:
        sensor.detach()
    except usb.core.USBError:  # on Linux may not have sufficient permissions
        raise IOError("You are probably on Linux. Ensure you"
                      "have rights enabled to manage USB devices"
                      "or create appropriate udev rules")

    delay = 1. / frequency

    while not stop_trigger:
        x1, y1 = sensor.get_position_change()
        x.value += x1
        y.value += y1
        dx.value = x1
        dy.value = y1


        time.sleep(delay)


def launch_app(frequency, x, y, dx, dy, stop_trigger):
    def on_destroy():
        stop_trigger.value = True
        app.master.destroy()

    def on_key(event):
        if event.keycode == 9 or event.keycode == 27:  # escape event Linux/Win
            on_destroy()

    def coords_update():
        app.x.set(x.value)
        app.y.set(y.value)
        app.dx.set(dx.value)
        app.dy.set(dy.value)

        app.master.after(delay, coords_update)

    delay = int(100/frequency)

    app = DemoApp()
    app.master.bind("<Key>", on_key)
    app.master.protocol("WM_DELETE_WINDOW", on_destroy)

    coords_update()
    app.master.mainloop()


if __name__ == "__main__":
    """
    Usage example:
    linux               python demo.py '{"idVendor": 1133, "idProduct": 49177}'
    or win              python .\demo.py '{""idVendor"": 1133, ""idProduct"": 49177}'
    """
    try:
        data = sys.argv[1]
        devices = Mouse.list_connected(**json.loads(data))
    except IndexError:
        devices = Mouse.list_connected()

    try:
        m1 = devices[1]
        kwargs = {
            'bus': m1.usb_device.bus,
            'address': m1.usb_device.address
        }
    except IndexError:
        raise IOError("No connected mice found.")

    x = Value('d', 0.0, lock=False)
    y = Value('d', 0.0, lock=False)
    dx = Value('d', 0.0, lock=False)
    dy = Value('d', 0.0, lock=False)
    stop_trigger = Value(ctypes.c_bool, False, lock=False)

    mouse_process = Process(target=sensor_handler, args=(kwargs, 100, x, y, dx, dy, stop_trigger))
    app_process = Process(target=launch_app, args=(50, x, y, dx, dy, stop_trigger))

    mouse_process.start()
    app_process.start()

    mouse_process.join()
    app_process.join()
