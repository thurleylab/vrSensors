from setuptools import setup, find_packages
import os


__author__ = 'asobolev'


setup(
    name='sensors',
    version='0.1',
    description='Some wrapping to control optical mouse sensor inputs',
    author='Andrey Sobolev',
    author_email='sobolev@bio.lmu.de',
    packages=find_packages(),
    include_package_data=True,
    package_data={'': ['devices.json']},
    install_requires=['pyusb']
)
